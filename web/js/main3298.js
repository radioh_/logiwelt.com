var _root = $('#root').val();

$(document).on('ready',function()
{
    var header = $('header');

    $('.validate').validate();
    $('.validate_newsletter').validate();
    $('.validate_contact').validate();

    new WOW().init();

	//Desplazamiento
	function moveto(tag){
		$('html,body').animate({
		    scrollTop: ($("#"+tag).offset().top - header.height())
		}, 1000);
	}

	$('.page-scroll').click(function()
    {
        // $('.page-scroll').removeClass('active_page');
        // $(this).addClass('active_page');
		var go = $(this).attr('go-to');
		moveto(go);
	});

	

    $(function(){
        $('.horario .item-horario').on('click', function(){
            if (!$(this).hasClass('item-active')) {
                $('.horario .item-horario').removeClass('item-active');
                $(this).addClass('item-active');
            }
            else{
                $(this).removeClass('item-active');
            }
        });
    });

    $(function(){
        $('.head-item').on('click', function(){
            if (!$(this).hasClass('active')) {
                $('.head-item').removeClass('active');
                $(this).addClass('active');

                var option = $(this).data('option');
                $('.body-item').slideUp('active');
                $('.body-'+option).slideDown('active');
            }
        });
    });

    var start = $('.slider_home').offset();
    var about      = $('.what-do').offset();
    var services   = $('.services').offset();
    var contact    = $('.contacto').offset();

    //console.log(contact);
    $(document).on('scroll', function(){
        var height = $(document).scrollTop();

        if (height > 1) {
            header.addClass('header-scroll');
        }else {
            header.removeClass('header-scroll');
        }

        if (height > (start.top - header.height()) && height < (about.top - header.height())) {
            $('.page-scroll').removeClass('active_page');
            $('.nav_home').addClass('active_page');
        }
        else if (height >= (about.top - header.height()) && height < (services.top - header.height() - 15)) {
            $('.page-scroll').removeClass('active_page');
            $('.nav_about').addClass('active_page');
        }
        else if (height >= (services.top - header.height() - 15) && height < (contact.top - header.height())) {
            $('.page-scroll').removeClass('active_page');
            $('.nav_services').addClass('active_page');
        }
        else if (height >= (contact.top - header.height())) {
            $('.page-scroll').removeClass('active_page');
            $('.nav_contact').addClass('active_page');
        }

    });

    var rellax = new Rellax('.rellax', {center: true});

	$('body').on('keydown', '.digits', function(event) {
        if(event.shiftKey) {
           event.preventDefault();
        }

       if (event.keyCode != 46 && event.keyCode != 8) {
            if (event.keyCode < 95) {
                if (event.keyCode < 48 || event.keyCode > 57) {
                   event.preventDefault();
                }
            } else {
                if (event.keyCode < 96 || event.keyCode > 105) {
                   event.preventDefault();
                }
            }
        }
    });

});
