const gulp = require('gulp'),
purgecss = require('gulp-purgecss'),
minifyjs = require('gulp-js-minify'),
autoprefixer = require('gulp-autoprefixer'),
runSequence = require('gulp4-run-sequence').use(gulp),
cleanCSS = require('gulp-clean-css'),
htmlmin = require('gulp-htmlmin'),
image = require('gulp-image'),
minify = require('gulp-minify');
 



gulp.task('default', async function(){ 
    runSequence(['CSS-purge','JS-minify','IMG-optimize','HTACCESS-copy','HTML-minify'],'CSS-minify','CONTENT-optimize');
});

gulp.task('JS-copy', async function() {
   gulp.src('./js/**/*.js')
   .pipe(gulp.dest('./build/js/'));
});


gulp.task('IMG-optimize', () => {
  gulp.src('./web/images/**/*.*')
    .pipe(image({
      optipng: ['-i 1', '-strip all', '-fix', '-o7', '-force'],
      pngquant: ['--speed=1', '--force', 256],
      zopflipng: ['-y', '--lossy_8bit', '--lossy_transparent'],
      jpegRecompress: ['--strip', '--quality', 'medium', '--min', 40, '--max', 80],
      mozjpeg: ['-optimize', '-progressive'],
      guetzli: ['--quality', 85],
      gifsicle: ['--optimize'],
      svgo: ['--enable', 'cleanupIDs', '--disable', 'convertColors']
    }))
    .pipe(gulp.dest('./build/web/images/'));
});


gulp.task('CONTENT-optimize', () => {
  gulp.src('./web/content/**/*.*')
    .pipe(image({
      optipng: ['-i 1', '-strip all', '-fix', '-o7', '-force'],
      pngquant: ['--speed=1', '--force', 256],
      zopflipng: ['-y', '--lossy_8bit', '--lossy_transparent'],
      jpegRecompress: ['--strip', '--quality', 'medium', '--min', 40, '--max', 80],
      mozjpeg: ['-optimize', '-progressive'],
      guetzli: ['--quality', 85],
      gifsicle: ['--optimize'],
      svgo: ['--enable', 'cleanupIDs', '--disable', 'convertColors']
    }))
    .pipe(gulp.dest('./build/web/content/'));
});
gulp.task('IMG-copy', async ()=> {
  gulp.src(['./web/images/**/*.*'])
    .pipe(minify())
    .pipe(gulp.dest('./build/web/images/'))
});


gulp.task('HTACCESS-copy', async ()=> {
  gulp.src(['./.htaccess'])
    .pipe(minify())
    .pipe(gulp.dest('./build/'))
});


gulp.task('HTML-minify', async ()=> {
  gulp.src(['./index.html'])
    .pipe(minify())
    .pipe(gulp.dest('./build/'))
});
 
gulp.task('JS-minify', async ()=> {
  gulp.src(['./web/js/**/*.js'])
    .pipe(minify())
    .pipe(gulp.dest('./build/web/js/'))
});


gulp.task('CSS-purge', async () => {
    return gulp.src('./css/**/*.css')
        .pipe(purgecss({
            content: ['./js/**/*.js','./**/*.php','./content/**/*.php']
        }))
        .pipe(gulp.dest('./tmp/css'))
})


gulp.task('CSS-minify', async() => {
  return gulp.src('./tmp/css/**/*.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('./build/css/'));
});


 
